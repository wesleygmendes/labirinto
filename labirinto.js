// Constantes do Labirinto


const caminhoLabirinto = document.getElementById("caminhoDoLabirinto");
const map = [
  "WWWWWWWWWWWWWWWWWWWWW",
  "W   W     W     W W W",
  "W W W WWW WWWWW W W W",
  "W W W   W     W W   W",
  "W WWWWWWW W WWW W W W",
  "W         W     W W W",
  "W WWW WWWWW WWWWW W W",
  "W W   W   W W     W W",
  "W WWWWW W W W WWW W F",
  "S     W W W W W W WWW",
  "WWWWW W W W W W W W W",
  "W     W W W   W W W W",
  "W WWWWWWW WWWWW W W W",
  "W       W       W   W",
  "WWWWWWWWWWWWWWWWWWWWW",
];


//Gerador de Mapa
let vertical = 9;
let horizontal = 0;
const geradorDeMapa = () => {
  for (let i = 0; i < map.length; i++) {
    let linha = document.createElement("div");
    linha.className = "linhas";
    for (let j = 0; j < map[i].length; j++) {
      let celulaBase = i + "." + j;
      if (map[i][j] === "W") {
        let parede = document.createElement("div");
        parede.className = "parede";
        parede.id = celulaBase;
        linha.appendChild(parede);
      } else if (map[i][j] === " ") {
        let caminho = document.createElement("div");
        caminho.className = "caminho";
        caminho.id = celulaBase;
        linha.appendChild(caminho);
      } else if (map[i][j] === "S") {
        let start = document.createElement("div");
        start.className = "start";
        start.id = celulaBase;
        linha.appendChild(start);
      } else if (map[i][j] === "F") {
        let fimDoJogo = document.createElement("div");
        fimDoJogo.className = "fim";
        fimDoJogo.id = celulaBase;
        linha.appendChild(fimDoJogo);
      }
    }
    caminhoLabirinto.appendChild(linha);
  }
};
geradorDeMapa();

// Movimentação do Player 

const movimentacaoPlayer = () => {
  let player = document.querySelector(".player"); //query seletor css sintaxe (element = document.querySelector(selectors);)
  let celula = vertical + "." + horizontal;
  let espacos = document.getElementById(celula);
  player.parentNode.removeChild(player);
  espacos.appendChild(player);
};


// Precisão de Movimento

const precisaoDeMovimento = (x, y) => {
  if (
    y >= 0 &&
    y < map.length &&
    x >= 0 &&
    x < map[0].length &&
    map[y][x] !== "W"
  ) {
    return true;
  }
  return false;
};



// Controles do Jogo
const andar = document.addEventListener("keydown", (event) => {
  let keyName = event.key;
  if (!vitoria()) {
    if (keyName === "ArrowRight" && precisaoDeMovimento(horizontal + 1, vertical)) {
      horizontal += 1;
    } else if (
      keyName === "ArrowLeft" &&
      precisaoDeMovimento(horizontal - 1, vertical)
    ) {
      horizontal -= 1;
    } else if (
      keyName === "ArrowUp" &&
      precisaoDeMovimento(horizontal, vertical - 1)
    ) {
      vertical -= 1;
    } else if (
      keyName === "ArrowDown" &&
      precisaoDeMovimento(horizontal, vertical + 1)
    ) {
      vertical += 1;
    }
  } else {
    return false;
  }
  movimentacaoPlayer();
  vitoria();
})




movimentacaoPlayer();




// Condição de Vitória



const vitoria = () => {
  let player = document.querySelector(".player");
  if (map[vertical][horizontal] == "F") {
    let vitoria = document.getElementById("image");
    vitoria.className = "vitoria";
    player.parentNode.removeChild(player);
    
    return true;
  }
  return false;
};
